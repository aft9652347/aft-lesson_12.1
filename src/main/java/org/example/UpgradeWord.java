package org.example;

import java.util.*;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.split;

public class UpgradeWord {
    //Метод для добавления в строку []
    public String getUpgradeWord(String newWord) {
//        try {
        if (newWord == null || newWord == ""){
            throw new NullPointerException("Строка не должна быть пустой или null, введите валидное значение");
        }
         String[] separationByWord = split(newWord);
        List<String> listUpgradeWord = new ArrayList<>();
        for (String s : separationByWord) {
            String dd = (format("[%s]", s));
            listUpgradeWord.add(dd);        }
        return listUpgradeWord.toString()
                .replace("[[", "[")
                .replace(",", "")
                .replace("]]", "]");
//    }catch (NullPointerException e){
//        throw new RuntimeException(e);
//        }
}

//    public String getUpgradeWord(String newWord) {
//        List<String> separationByWord = Arrays.asList(split(newWord));
//        List<String> listUpgradeWord = new ArrayList<>();
//        for (int i = 0; i < separationByWord.size(); i++){
//            String dd = (format("[%s]", separationByWord.get(i)));
//            listUpgradeWord.add(dd);
//        }
//        return listUpgradeWord.toString()
//                .replace("[[", "[")
//                .replace(",", "")
//                .replace("]]", "]");
//    }

}
