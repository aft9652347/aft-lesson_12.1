package org.example;

import org.apache.commons.lang3.StringUtils;

public class ReverseWord{
        //Метод для реверса строки
        public String getReverseWord(String word) {
          // try {
               if(word == null || word == ""){
                   throw new NullPointerException("Строка не должна быть пустой или null, введите валидное значение");
               }
//           }catch (NullPointerException nullException){
//            throw new NullPointerException();
//           }
           return StringUtils.reverse(word);
        }
}
