package ru.aft.lesson12;

import org.example.UpgradeWord;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UpgradeWordTest {
    //Создание объекта класса upgradeWord для работы с его методами
    UpgradeWord upgradeWord = new UpgradeWord();
    //Подготовка тестовых данных для параметризованного кейса согласно заданию
    @DataProvider(name = "dataUpgradeWordTest")
    public Object[][] dataProviderUpgradeWord() {
        return new Object[][]{
                {"пара слов", "[пара] [слов]"},
                {"слово", "[слово]"},
                {"еще немного слов", "[еще] [немного] [слов]"},

        };
    }
    //Подготовка тестовых данных для негативного параметризованного кейса
    @DataProvider(name = "dataNegativeTestUpgradeWord")
    public Object[] negativeDataProviderUpgradeWord(){
         return new Object[]{
                 null,
                 ""
          };
    }
    //Проверка по заданию
    @Test(dataProvider = "dataUpgradeWordTest", groups = {"smoke"})
    public void testUpgradeWord(String newWord, String expectedResult) {
            Assert.assertEquals(upgradeWord.getUpgradeWord(newWord), expectedResult);
    }
    //Проверка на Null и пустую строку, негативный кейс
    @Test(dataProvider = "dataNegativeTestUpgradeWord")
    public void test(String newWord) {
        Assert.assertThrows(NullPointerException.class, () -> upgradeWord.getUpgradeWord(newWord));
    }
}
