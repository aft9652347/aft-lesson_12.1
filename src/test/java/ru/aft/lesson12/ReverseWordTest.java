package ru.aft.lesson12;

import org.example.ReverseWord;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ReverseWordTest {
    //Создание объекта класса reverseWord для работы с его методами
    ReverseWord reverseWord = new ReverseWord();
    //Подготовка тестовых данных для параметризованного кейса согласно заданию
    @DataProvider(name = "dataValidReverseWord")
    public Object[][] dataProviderWord(){
        return new Object[][]{
                {"раз", "зар"},
                {"я устал придумывать примеры", "ыремирп ьтавымудирп латсу я"}
        };
    }
    //Подготовка тестовых данных для негативного параметризованного кейса
    @DataProvider(name = "negativeDataReverseWord")
    public Object[] negativeDataProviderWord(){
        return new Object[]{
                null,
                ""
        };
    }
    //Проверки по заданию
    @Test (dataProvider = "dataValidReverseWord", groups = "smoke")
    public void reverseWordTest(String word, String expectedResult){
        Assert.assertEquals(reverseWord.getReverseWord(word), expectedResult);
    }
    //Негативные кейсы null b ""
    @Test (dataProvider = "negativeDataReverseWord")
    public void reverseWordNegativeTest(String word) {
        Assert.assertThrows(NullPointerException.class, () -> reverseWord.getReverseWord(word));
    }
}
